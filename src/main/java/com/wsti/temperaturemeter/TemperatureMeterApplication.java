package com.wsti.temperaturemeter;

import com.wsti.temperaturemeter.domain.CelsiusTemperatureDisplay;
import com.wsti.temperaturemeter.domain.CelsiusTemperatureMeter;
import com.wsti.temperaturemeter.domain.FahrenheitTemperatureDisplay;
import com.wsti.temperaturemeter.domain.KelvinTemperatureDisplay;

import java.util.Scanner;

public class TemperatureMeterApplication {

    private static final String NUMBER_REGEXP = "-?\\d+(\\.\\d+)?";
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        CelsiusTemperatureMeter temperatureMeter = new CelsiusTemperatureMeter();
        temperatureMeter.registerObserver(new CelsiusTemperatureDisplay());
        temperatureMeter.registerObserver(new KelvinTemperatureDisplay());
        temperatureMeter.registerObserver(new FahrenheitTemperatureDisplay());

        System.out.println("Welcome to TemperatureMeter!");

        while (true) {
            System.out.print("Enter celsius temperature: ");
            String input = SCANNER.nextLine();

            if (!input.matches(NUMBER_REGEXP)) {
                break;
            }
            temperatureMeter.setTemperatureValue(Double.parseDouble(input));
            System.out.println("\n");
        }
    }
}
