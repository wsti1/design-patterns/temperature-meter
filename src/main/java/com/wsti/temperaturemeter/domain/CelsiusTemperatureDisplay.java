package com.wsti.temperaturemeter.domain;

public class CelsiusTemperatureDisplay implements Observer {

    private double temperatureValue;

    @Override
    public void update(double temperatureValue) {
        this.temperatureValue = temperatureValue;
        print();
    }

    public void print() {
        System.out.println("[Celsius temperature display]: " + temperatureValue + " °C");
    }
}
