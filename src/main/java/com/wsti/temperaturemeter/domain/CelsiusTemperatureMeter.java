package com.wsti.temperaturemeter.domain;

import java.util.ArrayList;
import java.util.List;

public class CelsiusTemperatureMeter implements Subject {

    private double temperatureValue;
    private List<Observer> observers;

    public CelsiusTemperatureMeter() {
        this.temperatureValue = 0;
        this.observers = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void unregisterObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(observer -> observer.update(temperatureValue));
    }

    public void setTemperatureValue(double temperatureValue) {
        this.temperatureValue = temperatureValue;
        notifyObservers();
    }
}
