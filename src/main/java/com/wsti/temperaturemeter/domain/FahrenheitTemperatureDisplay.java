package com.wsti.temperaturemeter.domain;

public class FahrenheitTemperatureDisplay implements Observer {

    private double temperatureValue;

    @Override
    public void update(double temperatureValue) {
        this.temperatureValue = (temperatureValue * 1.8) + 32;
        print();
    }

    public void print() {
        System.out.println("[Fahrenheit temperature display]: " + temperatureValue + " °F");
    }
}
