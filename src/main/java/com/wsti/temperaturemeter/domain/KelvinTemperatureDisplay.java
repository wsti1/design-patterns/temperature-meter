package com.wsti.temperaturemeter.domain;

public class KelvinTemperatureDisplay implements Observer {

    private double temperatureValue;

    @Override
    public void update(double temperatureValue) {
        this.temperatureValue = temperatureValue + 273.15;
        print();
    }

    public void print() {
        System.out.println("[Kelvin temperature display]: " + temperatureValue + " K");
    }
}
