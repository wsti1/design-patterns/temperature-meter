package com.wsti.temperaturemeter.domain;

public interface Observer {

    void update(double temperatureValue);
}
